///////////////////////////////////////////////////////////////////////////////
/////          University of Hawaii, College of Engineering
///// @brief   Lab 07c - My First Cat - EE 205 - Spr 2022
/////
///// @file    hello2.cpp
///// @version 1.0 - Initial implementation
/////
///// Build and test 3 Hello World Programs
/////
///// @author  Carl Domingo <carld20@hawaii.edu>
///// @@date   02/25/2022
/////
///// @see     https://www.gnu.org/software/make/manual/make.html
/////////////////////////////////////////////////////////////////////////////////

#include <iostream>

class Hello{
	public:
		void hello();
};

void Hello::hello(void){
	std::cout << "Hello World!!" << std::endl;
}


int main() {
	
	Hello greet;
	greet.hello();
	return 0;

}
